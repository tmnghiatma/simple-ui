package com.example.simpleui.model

import org.springframework.data.elasticsearch.annotations.Document

@Document(indexName = "daily_covid_info")
data class CovidInfo(
    private val country: String,
    private val cases: String,
    private val deaths: String,
    private val recovered: String,
    private val date: String
)
