package com.example.simpleui.controller

import com.example.simpleui.model.FilterItem
import com.example.simpleui.service.CovidInfoService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping

@Controller
class CovidInfoController(@Autowired val covidInfoService: CovidInfoService) {
    private val logger = LoggerFactory.getLogger(javaClass)

    @RequestMapping("/")
    fun index(model: Model): String {
        model.addAttribute("covidinfos", covidInfoService.getAll())
        return "covidinfo"
    }

    @PostMapping("/filterbydate")
    fun handleFilter(@ModelAttribute("String") filterItem: FilterItem, model: Model): String {
        logger.info("date: {}", filterItem.date)
        model.addAttribute("covidinfos", covidInfoService.getByDate(filterItem.date))

        return "covidinfo"
    }
}