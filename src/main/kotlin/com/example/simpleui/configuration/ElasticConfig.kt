package com.example.simpleui.configuration

import org.elasticsearch.client.RestHighLevelClient
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment
import org.springframework.data.elasticsearch.client.ClientConfiguration
import org.springframework.data.elasticsearch.client.RestClients
import org.springframework.data.elasticsearch.config.AbstractElasticsearchConfiguration

@Configuration
class ElasticConfig(@Autowired var env: Environment) : AbstractElasticsearchConfiguration() {
    private val logger = LoggerFactory.getLogger(javaClass)

    //    @Bean
    override fun elasticsearchClient(): RestHighLevelClient {
        val elasticUri = env.getProperty("elastic.uri", "localhost:9200")
        val clientConfiguration = ClientConfiguration.builder().connectedTo((elasticUri)).build()

        return RestClients.create(clientConfiguration).rest()
    }
}