package com.example.simpleui.service

import com.example.simpleui.model.CovidInfo
import com.google.gson.Gson
import org.elasticsearch.action.search.SearchRequest
import org.elasticsearch.client.RequestOptions
import org.elasticsearch.client.RestHighLevelClient
import org.elasticsearch.index.query.QueryBuilders
import org.elasticsearch.search.builder.SearchSourceBuilder
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CovidInfoService(@Autowired var elasticClient: RestHighLevelClient) {
    private val logger = LoggerFactory.getLogger(javaClass)

    fun getAll(): ArrayList<CovidInfo> {
        val covidInfoList = ArrayList<CovidInfo>()
        try {
            val request = SearchRequest("daily_covid_info")
            val searchSourceBuilder = SearchSourceBuilder().query(QueryBuilders.matchAllQuery()).size(1000)
            request.source(searchSourceBuilder)
            val result = elasticClient.search(request, RequestOptions.DEFAULT)
            result.hits.hits.forEach { item ->
                logger.info("==== item: {}", item)
                val gson = Gson()
                val covidInfo = gson.fromJson(item.sourceAsString, CovidInfo::class.java)
                logger.info("Info: {}", covidInfo)
                covidInfoList.add(covidInfo)
            }
        } catch (e: Exception) {
            logger.error(e.message)
            logger.debug(e.stackTraceToString())
        }

        return covidInfoList
    }

    fun getByDate(date: String): ArrayList<CovidInfo> {
        val covidInfoList = ArrayList<CovidInfo>()
        try {
            val request = SearchRequest("daily_covid_info")
            val searchSourceBuilder = SearchSourceBuilder().query(QueryBuilders.matchQuery("date", date)).size(1000)
            request.source(searchSourceBuilder)
            val result = elasticClient.search(request, RequestOptions.DEFAULT)
            result.hits.hits.forEach { item ->
                logger.info("==== item: {}", item)
                val gson = Gson()
                val covidInfo = gson.fromJson(item.sourceAsString, CovidInfo::class.java)
                logger.info("Info: {}", covidInfo)
                covidInfoList.add(covidInfo)
            }
        } catch (e: Exception) {
            logger.error(e.message)
            logger.debug(e.stackTraceToString())
        }

        return covidInfoList
    }

}